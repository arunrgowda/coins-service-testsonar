'use-strict';

module.exports = function(sequelize, DataTypes) {
  var paytmTransaction = sequelize.define('paytmTransaction', {
    patientId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull : false
    },
    orderId: {
      type: DataTypes.STRING,
      allowNull : false
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: DataTypes.STRING,
      allowNull : false,
      values : ['TRANSACTION','VERIFY']
    },
    status: {
      type : DataTypes.STRING,
      defaultValue : null
    },
    statusCode : {
      type : DataTypes.STRING,
      defaultValue : null
    },
    message: {
      type : DataTypes.STRING,
      defaultValue : null
    },
    walletTxnId: {
      type : DataTypes.STRING,
      defaultValue : null
    }
  },{
    classMethods: {
      associate: function(models) {

      }
    }
  });
  return paytmTransaction;
};
