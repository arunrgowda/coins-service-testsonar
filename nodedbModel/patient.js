"use strict";

module.exports = function(sequelize, DataTypes) {
  var Patient = sequelize.define("Patient", {
  imei:  DataTypes.STRING ,
  email: DataTypes.STRING,
  location : DataTypes.TEXT,
  gcm : DataTypes.STRING,
  platform : DataTypes.STRING,
	meta : DataTypes.TEXT,
	version : DataTypes.STRING,
  platform_version : DataTypes.STRING,
  username : DataTypes.STRING,
  password : DataTypes.STRING,
  device : DataTypes.STRING,
  uuid : DataTypes.STRING ,
  name : DataTypes.STRING,
  phonenumber : DataTypes.STRING,
  city : DataTypes.STRING,
  registerEmail : DataTypes.STRING,
  validGcm : {type:DataTypes.BOOLEAN , defaultValue :0},
  age : DataTypes.STRING,
  gender : DataTypes.STRING,
  registeredPhonenumber : DataTypes.STRING,
  address : DataTypes.TEXT,
  wallet : DataTypes.DECIMAL,
  source : DataTypes.STRING,
  docsMateDoctor : DataTypes.STRING,
  thyrocare : DataTypes.BOOLEAN,
  bajaj : DataTypes.BOOLEAN,
  priority : DataTypes.STRING,
  appviralityUserKey : DataTypes.STRING,
  advertiserId : DataTypes.STRING,
  appsFlyerId : DataTypes.STRING,
  simplAllowed : DataTypes.INTEGER,
  adhar : DataTypes.STRING,
  isMedAllUser : DataTypes.INTEGER,
  language : DataTypes.STRING,
  parentId: DataTypes.INTEGER(11)
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  Patient.associate = models => {
    
  };

  return Patient;
};
