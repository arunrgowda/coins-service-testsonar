"use strict";

module.exports = function(sequelize, DataTypes) {
  var couponCode = sequelize.define("couponCode", {
    name: DataTypes.STRING,
    code: DataTypes.STRING, 
    discount: DataTypes.STRING,
    couponRule: DataTypes.STRING,
    reusable: DataTypes.STRING,
    globalReusable: DataTypes.INTEGER,
    firstOnly : DataTypes.STRING,
    active : DataTypes.STRING,
    topic : DataTypes.STRING,
    invalidTopic : DataTypes.STRING,
    validTill : DataTypes.DATE,
    serviceType : DataTypes.STRING,
    maximum : DataTypes.STRING,
    meta : DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
       
      }
    }
  });

  return couponCode;
};