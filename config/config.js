const env = process.env.NODE_ENV || 'test'; // 'dev' or 'test'
var extend = require('util')._extend;

var app_config = require('./credentials/' + env + '_app_config.json');

var config = {};

config = extend(config, app_config);

//console.log(JSON.stringify(config,'',2));

module.exports = config;