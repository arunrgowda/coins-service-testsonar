
var crypt = require('./crypt');
var crypto = require('crypto');


function genchecksumbystring(params, key, cb) {

    crypt.gen_salt(4, function (err, salt) {
      var sha256 = crypto.createHash('sha256').update(params + '|' + salt).digest('hex');
      var check_sum = sha256 + salt;
      var encrypted = crypt.encrypt(check_sum, key);
  
      var CHECKSUMHASH = encodeURIComponent(encrypted);
      CHECKSUMHASH = encrypted;
      cb(undefined, CHECKSUMHASH);
    });
  }
  module.exports.genchecksumbystring = genchecksumbystring;
