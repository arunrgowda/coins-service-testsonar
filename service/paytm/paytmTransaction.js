const uuidv1 = require('uuid/v1');
const httpStatus = require('http-status-codes');
var request = require("request");
const checkSum = require('./checksum');
const models = require("../../models");
const nodeDbModels = models['nodeDb'];

const paytmConfig = require('../../config/config')['paytm'];
const {makeTransaction} = require('../../helpers/walletTransaction');
const paytmTransferPercentage = 100;
const checksumError = 'checksumError';
const paytmError = 'paytmError';
const internalServerError = 'internalError';
const tryAgainError = 'tryAgain';
const notRegisteredError = 'notRegistered';
const walletVerificationError = 'notVerified';
const duplicateOrderIdError = 'duplicateOrderId';
const mobileVerficationError = 'mobileNotVerified';
const emailVerficationError = 'emailNotVerified';
const mobileAndEmailVerficationError = 'mobileAndEmailNotVerified';
const walletActivationError = 'walletNotActivated';
const maxTransferLimitError = 'maxAmount';
const noTransferAmountError = 'noTransferAmount';
const invalidNumber = 'invalidNumber';
const multipleAccountsError = 'multipleUnverifiedAccounts';
const balanceFail ="balanceFail";



/**
 * Summary                              used for either creating or updating an entry in paytmTransaction table
 * @param   {Object}   paytmObject      object required to create entry in paytmTransaction table
 * @param   {String}   switchAction     used for switching function to either create or update a paytm transaction
 * @param   {Object}   whereClause      selection statement for an entry , required only in case of update ,OPTIONAL
 * @returns {Promise}
 * @returns {Object}                    created object  in case of creation or blank in case of updation
 */
let createOrUpdatePaytmTransaction = (paytmObject , switchAction , whereClause) => {
  if(switchAction === 'CREATE'){
    console.info('Creating paytm Transaction entry');
    console.info(paytmObject);
    return nodeDbModels.paytmTransaction.create(paytmObject);
  }else if( switchAction === 'UPDATE'){
    console.info('Updating paytm Transaction entry for orderId' + JSON.stringify(whereClause));
    console.info(paytmObject);
    return nodeDbModels.paytmTransaction.update(paytmObject,
      {
        returning : true,
        where : whereClause
      });
  }
};

/**
 * Summary                              creates the required params to create a transaction on PayTm server
 * @param   {Object}   rawPaytmObject   paytmTransaction table entry record
 * @param   {String}   requestType      used for switching between verification and a normal transaction
 * @returns {Promise}
 * @returns {Object}                    request options to be sent to PayTm server
 * @throws  {String}                    checkSumError constant field
 */
let generatePostOptions = (rawPaytmObject , requestType) =>{
  return new Promise( (resolve , reject) => {
    let body = {
      request :{
        requestType : requestType === 'TRANSACTION' ? null : requestType,
        merchantGuid : paytmConfig.merchantGuid,
        merchantOrderId : rawPaytmObject.orderId,
        salesWalletName : paytmConfig.salesWalletName,
        salesWalletGuid : paytmConfig.salesWalletGuid,
        payeeEmailId : null,
        payeePhoneNumber : rawPaytmObject.phoneNumber,
        appliedToNewUsers : 'N',
        amount : rawPaytmObject.amount.toString(),
        payeeSsoId: null,
        currencyCode : 'INR'
      },
      metadata :rawPaytmObject.metadata||'Paytm Referral Transfer',
      ipAddress : '127.0.0.1',
      platformName : 'PayTM',
      operationType : 'SALES_TO_USER_CREDIT',
    };
    checkSum.genchecksumbystring(JSON.stringify(body), paytmConfig.merchantKey , (err , params) => {
      if(err){
        console.error('Error encountered while generating checksum in verifyNumber API');
        console.error(err);
        reject(checksumError);
        return;
      }
      let options = {
        method : 'POST',
        url : paytmConfig.url,
        headers : {
          'content-type': 'application/json',
          'MID' : paytmConfig.merchantId,
          'Checksumhash' : params
        },
        body : JSON.stringify(body)
      };
      resolve(options);
    });
  });
};
/**
 * Summary                              promisified function to make a request
 * @param   {Object}   requestBody      request options as passed to request module
 * @return  {Promise}
 * @return  {Object}                    statuscode and body of the response received
 * @throws  {Object}                    error encountered while making the request
 */
const makeRequest = (requestBody) => {
  return new Promise((resolve , reject) => {
    request(requestBody, (err , response , body) => {
      if (err)
        reject(err);
      resolve ({
        statusCode: response.statusCode,
        body: body
      });
    });
  });
};
/**
 * Summary                              sends request to PayTm server and updates the transaction with response
 * @param   {Object}   postOptions      post options to be sent to PayTm server
 * @returns {Promise}
 * @returns {Object}                    response returned from PayTm server
 * @throws  {String}                    paytmError constant field
 */
let requestAndUpdateStatusTransaction = (postOptions) => {
  return new Promise( (resolve , reject) => {
    let orderId = JSON.parse(postOptions.body).request.merchantOrderId;
    let receivedResponse;
    console.info('Requesting paytm for transaction with options : ' + JSON.stringify(postOptions));

    makeRequest(postOptions).then( response => {
      let body = JSON.parse(response.body);
      console.info('Received response from paytm : ' + JSON.stringify(response),"body  "+ JSON.stringify(body));
      let updatePaytmObject = {
        status: body.status,
        statusCode : body.statusCode,
        message: body.statusMessage,
        walletTxnId: body.response ? body.response.walletSysTransactionId : null
      };
      let where = {
        orderId: orderId
      };
      receivedResponse = {
        status: updatePaytmObject.status,
        statusCode : updatePaytmObject.statusCode,
        message : updatePaytmObject.message
      };
      if(updatePaytmObject.status === 'PENDING'){
        receivedResponse.status = "PENDING";
        receivedResponse.statusCode = "PENDING";
      }
      console.info('Updating status of transaction : ' + where.orderId);
      console.info(updatePaytmObject);
      return createOrUpdatePaytmTransaction(updatePaytmObject,'UPDATE',where);
    })
    .then(() => {
      console.info('Updated status of transaction : ' + orderId);
      resolve(receivedResponse);
    })
    .catch(err => {
      console.error('Error encountered while contacting paytm in verifyNumber API');
      console.error(err);
      bugsnag.notify(err);
      reject(paytmError);
    });
  });
};

/**
 * Summary                              used to return the appropriate error according to a transaction(if any)
 * @param   {Object}   serverResponse   parsed response returned from Paytm server
 * @returns {String}                    returns suitable error or blank if no error occured
 */
let checkServerResponse = (serverResponse) =>{
  let error = null;
  //Mapping of status codes
  /*let paytmCodesMapping = {
    SUCCESS : 'SUCCESS',
    GE_1032 :  'INVALID PHONE NUMBER'
    GE_1033 :  'MULTIPLE ACCOUNTS FOR PHONE NUMBER'
    GE_1044 : 'ACCEPTED',
    ACCEPTED : 'ACCEPTED',
    OEC-1002 : 'PAYTM ERROR , TRY AGAIN LATER'
    STUC_1001 : 'NOT REGISTERED',
    STUC_1002 : 'WALLET NOT VERIFIED',
    STUC_1007 : 'ORDER-ID ALREADY EXISTS',
    STUC_1010 : 'MOBILE NOT VERIFIED',
    STUC_1011 : 'EMAIL NOT VERIFIED',
    STUC_1012 : 'MOBILE AND EMAIL NOT VERIFIED',
    STUC_1024 : 'WALLET NOT ACTIVATED',
    RWL_3002 : KYC NOT DONE
  };*/
  switch(serverResponse.statusCode){
    case 'SUCCESS':
    case 'ACCEPTED':
    case 'GE_1044':
    case 'PENDING':
      break;
    case 'GE_1032':
      error = invalidNumber;
      break;
    case 'GE_1033':
      error = multipleAccountsError;
      break;
    case 'OEC-1002':
      error = tryAgainError;
      break;
    case 'STUC_1001':
      error = notRegisteredError;
      break;
    case 'STUC_1002':
    case 'RWL_3002':
      error = walletVerificationError;
      break;
    case 'STUC_1007':
      error = duplicateOrderIdError;
      break;
    case 'STUC_1010':
      error = mobileVerficationError;
      break;
    case 'STUC_1011':
      error = emailVerficationError;
      break;
    case 'STUC_1012':
      error = mobileAndEmailVerficationError;
      break;
    case 'STUC_1024':
      error = walletActivationError;
      break;    
    case 'WM_1006':
      error = balanceFail;
      break;
    default :
      error = internalServerError;
  }

  return error;
};

/**
 * Summary                              used to create a negative wallet transaction for a user
 * @param   {Object}   data             object required to create a negative walletTransaction
 * @returns {Promise}
 * @returns {Object}                    indicated wether wallet transaction entry was created successfully
 */
let deductWalletAmount = (data) => {

  return makeTransaction(data.patientId,data.amount,"DocsApp Wallet Transaction for paytm");
};

exports.verifyNumberAndTransfer = async(variables)=>{
  return new Promise( (resolve , reject) => {
    let paytmTransaction = {
      patientId : variables.patientId,
      phoneNumber : variables.phoneNumber ,
      orderId : uuidv1(),
      type : 'VERIFY',
      amount:(variables.amount/100)*paytmTransferPercentage
    };
 createOrUpdatePaytmTransaction(paytmTransaction,'CREATE' , null)
    .then( verifyPaytmTransaction => {
      console.info('Completed creating paytm transaction entry');
      console.info('Generating verify request options');
      verifyPaytmTransaction.metadata="DocsApp reward cash";
      return generatePostOptions(verifyPaytmTransaction, 'VERIFY');
    })
    .then( verifyPostOptions => {
      console.info('Completed generating verify request options');
      console.info(verifyPostOptions);
      console.info('Checking paytm for phone number');
      return requestAndUpdateStatusTransaction(verifyPostOptions);
    })
    .then( (serverResponse) => {
      let error = checkServerResponse(serverResponse);
      if(error){
        console.error('Error in verification request from Paytm for phoneNumber : ' + paytmTransaction.phoneNumber);
        throw error;
      }
      let newPaytmTransaction = {};
      Object.assign(newPaytmTransaction,paytmTransaction);
      newPaytmTransaction.orderId = uuidv1();
      newPaytmTransaction.type = 'TRANSACTION';
      delete newPaytmTransaction.status;
      delete newPaytmTransaction.message;
      delete newPaytmTransaction.walletTxnId;
      console.info('Updated transaction status');
      return createOrUpdatePaytmTransaction(newPaytmTransaction,'CREATE' , null)
    })
    .then( transferPaytmTransaction => {
      console.info('Completed creating paytm transaction entry');
      console.info('Generating transaction request options');
      transferPaytmTransaction.metadata="DocsApp reward cash";
      return generatePostOptions(transferPaytmTransaction, null);
    })
    .then( transferPostOptions => {
      console.info('Completed generating transaction request options');
      console.info('Transferring cash to paytm for phone number : ' + paytmTransaction.phoneNumber);
      return requestAndUpdateStatusTransaction(transferPostOptions);
    })
    .then( (serverResponse) => {
      let error = checkServerResponse(serverResponse);
      if(error){
        console.error('Error in transaction request from Paytm for phoneNumber : ' + paytmTransaction.phoneNumber);
        throw error;
      }
      console.info('Transferred cash to paytm for phone number : ' + paytmTransaction.phoneNumber);
      console.info('Creating wallet transaction');
      return deductWalletAmount({
        patientId : variables.patientId,
        amount : variables.amount * -1
      });
    })
    .then( (walletInfo) => {
      if(walletInfo.success === 1)
        console.info('Completed creating wallet transaction');
      else{
        console.error('Error creating wallet transaction');
        console.error(walletInfo.status)
      }
      //Check to see if success if to be returned always
      resolve({
        statusCode : httpStatus.OK,
      });
    })
    .catch(err => {
      let message;
      if(err === notRegisteredError || err === walletVerificationError || err === mobileVerficationError
        || err === emailVerficationError || err === mobileAndEmailVerficationError || err === walletActivationError ||
        err === maxTransferLimitError || noTransferAmountError){
        message = {
          statusCode : httpStatus.NOT_ACCEPTABLE,
          error : err
        };
      }else if(err === duplicateOrderIdError){
        message = {
          statusCode : httpStatus.NOT_ACCEPTABLE,
          error : tryAgainError
        };
      }else if(err === checksumError){
        message = {
          statusCode : httpStatus.ACCEPTED,
          error : tryAgainError
        };
      }else if(err === paytmError){
        message = {
          statusCode : httpStatus.INTERNAL_SERVER_ERROR,
          error : tryAgainError
        };
      }else{
        console.error('Encountered unhandled error in verifyNumber API');
        console.error(err);
        message = {
          statusCode : httpStatus.INTERNAL_SERVER_ERROR,
          error : internalServerError
        };
      }
      resolve(message);
    });
  });
}
