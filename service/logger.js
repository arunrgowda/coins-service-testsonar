var os = require("os");
var fs = require('fs'),
    path = require('path');
// var loggly = require('loggly');
var winston = require('winston');require('winston-loggly');

  // var client = loggly.createClient({
  //   token: "",
  //   subdomain: "",
  //   auth: {
  //     username: "",
  //     password: ""
  //   },
  //   json : true,
  //   //
  //   // Optional: Tag to send with EVERY log message
  //   //
  //   tags: ['AutomationLogs']
  // });
var customLogger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)(),
      new (winston.transports.DailyRotateFile)({ name:'info' ,filename: 'debugFile'+os.hostname()+'.log' ,level : 'info'})
    ]
  });
  // var elkClient = require('./postToElastic');

//   var config = {
//   subdomain: "docsapp",
//   token : '9feefd03-d058-494e-8558-2869c39b99f6',
//   auth: {
//     username: "shreyas",
//     password: "123456release"
//   },
//   level : 'ERROR',
//   inputName : 'test'
// };
// winston.add(winston.transports.Loggly, config);
var criticalEvents = ["contentNewPatient","contentOldPatientNewTopic","contentOldPatientOldTopicNewConversation","contentDoctor","paymentCompleteNew","patientDefaultStartConsultation","dialogEndEvent","dashboardAssignDoctor","createNewConsult"];

var out = {};
  out["info"] = function(message,jsonObject){
  	if(jsonObject && jsonObject.toString() === "[object Object]")
  		jsonObject["machine"] = os.hostname();
    var logObject = jsonObject;

    if(!jsonObject)
      logObject = { info : message }
    // elkClient.execute(logObject,'AutomationInfo');
    customLogger.log(logObject);
    // client.log(logObject,['AutomationInfo'],function(err,data){
    //   if(err){
    //     console.log(err);
    //   }
    // });
  }
   out["log"] = function(message,jsonObject){
   //   var eventType = jsonObject && jsonObject.dataIn && jsonObject.dataIn.eventType;

  	// if(jsonObject && jsonObject.toString() === "[object Object]")
  	// 	jsonObject["machine"] = os.hostname();
   //  var logObject = jsonObject;

   //  if(!jsonObject)
   //    logObject = {log : message}
   //  // elkClient.execute(logObject,'AutomationLog');
   //  // customLogger.log(logObject);
   //  if(eventType && (criticalEvents.indexOf(eventType)!= -1))
      // client.log(jsonObject,['AutomationLogs'],function(err,data){
      //   if(err){
      //     console.log(err);
      //   }
      // });
  }
  out["error"] = function(message,jsonObject){
    var logObject = jsonObject;
    if(jsonObject && jsonObject.toString() === "[object Object]")
  		jsonObject["machine"] = os.hostname();
    if(!jsonObject)
      logObject ={error : message}
    // elkClient.execute(logObject,'AutomationLogsError');
    customLogger.log(logObject);
    // client.log(logObject,['AutomationLogsError'],function(err,data){
    //   if(err){
    //     console.log(err);
    //   }
    // });
  }
  out["action"] = function(message,jsonObject){
    var logObject = jsonObject;
    if(jsonObject && jsonObject.toString() === "[object Object]")
      jsonObject["machine"] = os.hostname();
    if(!jsonObject)
      logObject ={error : message}
    customLogger.log(logObject);
    // elkClient.execute(logObject,'AutomationAction');
    // client.log(logObject,['AutomationLogsError'],function(err,data){
    //   if(err){
    //     console.log(err);
    //   }
    // });
  }

  function cleanUpLogs(){
    var d = new Date();
    var month = d.getMonth() + 1;
    if(month < 10) month = "0" + month;
    var time =  d.getFullYear() +'-'+month+'-'+ d.getDate();
    var d = [ __dirname + '/../../logs/rewardSystemErrorFile'+os.hostname()+'.log.'+time ,
              __dirname + '/../../logs/rewardSystemWarnFile'+os.hostname()+'.log.'+time,
              __dirname + '/../../logs/rewardSystemInfoFile'+os.hostname()+'.log.'+time ,
              __dirname + '/../../logs/rewardSystemDebugFile'+os.hostname()+'.log.'+time ];
    for(var i in d){
    try { fs.unlinkSync(d[i]); }
    catch (ex) { console.log(ex)};
    }
  }
  customLogger.log({message:"test"})
  // cleanUpLogs();
  module.exports = out;
