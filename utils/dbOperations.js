const db = require('../models/index');

/**
 *  To create records via model
 *  @param {*} modelName tablename
 *  @param {*} record object of column name and values to be inserted into table
 *  return created record
 */
function createRecord(modelName, record, request) {
  model = modelName;
  record.createdAt = new Date();
  record.updatedAt = new Date();
  if (db[modelName]) {

    return db[modelName]
      .create(record, { request: request })
  } else {
    throw new Error(`Model ${model} not defined`);
  }
}

/**
 *  To create bulk records via model
 *  @param {*} modelName tablename
 *  @param {*} records array of objects of column name and values to be inserted into table
 *  return array of created record
 */

const createBulkRecords = async (modelName, records, request) => {
  if (db[modelName]) {
    return db[modelName]
      .bulkCreate(records, { individualHooks: true, request: request })
  } else {
    throw new Error(`Model ${model} not defined`);
  }
}

/**
 *  To update records via model
 *  @param {*} modelName tablename
 *  @param {*} query condition on basis of which records will be updated
 *  @param {*} record object of columnname and values to be updated in selected records
 *  @return no. of records updated
 */

function updateRecord(modelName, query, record, request) {
  query.request = request;
  record.updatedAt = new Date();
  if (db[modelName]) {
    db[modelName]
      .update(record, query)
  } else {
    throw new Error(`Model ${model} not defined`);
  }
}
/**
 *  To delete records via model
 *  @param {*} modelName table name
 *  @param {*} query condition on basis of which records will be deleted
 *  @return max no. of records deleted(0 or 1)
 */
function deleteRecord(modelName, query) {
  if (db[modelName]) {
    return db[modelName]
      .destroy(query)
  } else {
    throw new Error(`Model ${model} not defined`);
  }
}

/**
 *  To get records via model
 *  @param {*} modelName table name
 *  @param {*} query condition on basis of which records will be fetched
 *  @param isJoinTable if join is there
 *  @return return the records based on query
 */

async function getRawRecords(modelName, query, isJoinTable) {
  if (!isJoinTable) {
    query.raw = true; // to get plain records
  }
  if (db[modelName]) {
    let response = await db[modelName].findAll(query);
    if (isJoinTable) {
      response = JSON.stringify(response);
      response = JSON.parse(response);
    }
    return response;
  } else {
    return Promise.reject(new Error('Model not defined'));
  }
}

/**
 *  To get records via model by using select clause
 *  @param {*} modelName table name
 *  @param {*} query condition on basis of which records will be fetched
 *  @return return the records based on query
 */

function getRawRecordWithSelect(modelName, query, replacementObj) {
  query.raw = true; // to get plain records
  return new Promise((resolve, reject) => {
    db.sequelize
      .query(query, {
        raw: true,
        type: db.sequelize.QueryTypes.SELECT,
        replacements: { ...replacementObj }
      })
      .then((projects) => {
        // Each record will now be a instance of Project
        resolve(projects);
      })
      .catch((err) => {
        reject(new Error(err.message || 'Error while fetching record'));
      });
  });
}



/**
 *  To update recordif found else insert via model(upsert)
 *  @param {*} modelName tablename
 *  @param {*} record object of columnname and values to be updated in selected records
 *  @return no. of records updated
 */

function upsertRecord(modelName, record) {
  try {
    return db[modelName].upsert(record);
  } catch (error) {
    throw error;
  }
}

/**
 * This function returns the count of records with respect to passed query
 * @param {*} modelName
 * @param {*} query
 * @returns count of records
 */
function recordCount(modelName, query, include, column) {
  try {
    if (db[modelName]) {
      let option = { where: query };
      if (column) {
        option.distinct = true;
        option.col = column;
      }
      if (include) {
        option.include = include;
      }
      return db[modelName].count(option);
    } else {
      throw new Error('Model not defined');
    }
  } catch (e) {
    throw e;
  }
}
const createRecordInNodeDb =(modelName, record, request)=> {
  model = modelName;
  record.createdAt = new Date();
  record.updatedAt = new Date();
  if (db['nodeDb'][modelName]) {

    return db['nodeDb'][modelName]
      .create(record, { request: request })
  } else {
    throw new Error(`Model ${model} not defined`);
  }
}
/**
 *  To get records via model
 *  @param {*} modelName table name
 *  @param {*} query condition on basis of which records will be fetched
 *  @param isJoinTable if join is there
 *  @return return the records based on query
 */

async function getRawRecordsFromNodeDb(modelName, query, isJoinTable) {
  if (!isJoinTable) {
    query.raw = true; // to get plain records
  }
  if (db['nodeDb'][modelName]) {
    let response = await db['nodeDb'][modelName].findAll(query);
    if (isJoinTable) {
      response = JSON.stringify(response);
      response = JSON.parse(response);
    }
    return response;
  } else {
    return Promise.reject(new Error('Model not defined'));
  }
}
module.exports = {
  db,
  createRecord,
  updateRecord,
  deleteRecord,
  getRawRecords,
  createBulkRecords,
  getRawRecordWithSelect,
  upsertRecord,
  recordCount,
  createRecordInNodeDb,
  getRawRecordsFromNodeDb
};
