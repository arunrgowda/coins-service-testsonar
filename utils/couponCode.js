var voucherCodes = require('voucher-code-generator');
/* Function to generate n no. of coupon based on count */
const generateCouponCode = (count) => {
  let codes = voucherCodes.generate({
    length: 16,
    count: count
  });
  return codes;
}

module.exports = {
  generateCouponCode
}