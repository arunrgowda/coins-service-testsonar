/**
 * This function is used to add or subtract 
 * @param {date} date  from which date you want to add or subtract days
 * @param {integer} days  add or subtract no. of days from the given date
 */
const addDays = function (date, days) {
    try {
        var date = new Date(date);
        date.setDate(date.getDate() + days);
        return date;
    } catch (error) {
        console.error("Error in calculation date " + date, days);
        return date;
    }
}

module.exports = {
    addDays
}