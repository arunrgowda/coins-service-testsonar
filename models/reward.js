module.exports = function (sequelize, Sequelize) {
  var reward = sequelize.define(
    'reward',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      title: { type: Sequelize.STRING, allowNull: false },
      description: { type: Sequelize.TEXT, allowNull: false },
      steps: { type: Sequelize.TEXT, allowNull: true },
      purpose: { type: Sequelize.STRING(100), allowNull: false },
      validFrom: { type: Sequelize.DATE, allowNull: false },
      validTill: { type: Sequelize.DATE, allowNull: true },
      redirectUrl: { type: Sequelize.STRING, allowNull: true },
      imageUrl: { type: Sequelize.STRING, allowNull: false },
      colourCode: { type: Sequelize.STRING(50), allowNull: true },
      action: { type: Sequelize.STRING, allowNull: true },
      version: { type: Sequelize.STRING(20), allowNull: true },
      status: { type: Sequelize.INTEGER, allowNull: false },
      amount: { type: Sequelize.INTEGER, allowNull: false },
      usageLimit: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'rewards',
      underScored: false,
      timeStamps: false,
    },
  );
  reward.associate = function (models) {
    reward.hasMany(models.rewardsLanguageData)
  }
  return reward;
};
