module.exports = function (sequelize, Sequelize) {
  var rewardsLanguageData = sequelize.define(
    'rewardsLanguageData',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      languageCode: { type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      description: { type: Sequelize.TEXT, allowNull: false },
      steps: { type: Sequelize.TEXT, allowNull: true },
      rewardId: { type: Sequelize.BIGINT, allowNull: false },
      status: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'rewardsLanguageData',
      underScored: false,
      timeStamps: false,
    },
  );
  rewardsLanguageData.associate = function (models) {
    rewardsLanguageData.belongsTo(models.reward, {
      foreignKey: 'rewardId',
      target: 'id',
    })

  }
  return rewardsLanguageData
};
