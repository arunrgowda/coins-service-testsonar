module.exports = function (sequelize, Sequelize) {
  var coinsLanguageData = sequelize.define(
    'coinsLanguageData',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      languageCode: { type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      description: { type: Sequelize.STRING, allowNull: false },
      coinId: { type: Sequelize.BIGINT, allowNull: false },
      status: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'coinsLanguageData',
      underScored: false,
      timeStamps: false,
    },
  );
  coinsLanguageData.associate = function (models) {
    coinsLanguageData.belongsTo(models.coin, {
      foreignKey: 'coinId',
      target: 'id',
    })

  }
  return coinsLanguageData
};
