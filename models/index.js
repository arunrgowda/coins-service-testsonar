"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require("../config/config")["dbConfig"];
var nodeConfig = require("../config/config")["nodeDbConfig"];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var nodeSequelizeObject = new Sequelize(nodeConfig.database, nodeConfig.username, nodeConfig.password, nodeConfig);

var db = {};
db['nodeDb']={}
fs
  .readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function (file) {
    var model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function (modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});
sequelize.sync().then(function (d) {
  console.log('d');
}, function (err) {
  console.error(err);
});

fs
  .readdirSync(path.join(__dirname, '../nodedbModel'))
  .filter(function (file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function (file) {
    var model = nodeSequelizeObject["import"](path.join(__dirname, '../nodedbModel/'+file));
    db['nodeDb'][model.name] = model;
  });

Object.keys(db['nodeDb']).forEach(function (modelName) {
  if ("associate" in db['nodeDb'][modelName]) {
    db['nodeDb'][modelName].associate(db['nodeDb']);
  }
});
nodeSequelizeObject.sync().then(function (d) {
  console.log('d');
}, function (err) {
  console.error(err);
});

function bindOnUpdate(model, operation, db, user, options) {
  var diffChanges = deepdiff(user["dataValues"], user["_previousDataValues"]);
  return cacheServiceDashboard.execute(model, operation, db, user, options, diffChanges);
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.nodeSequelizeObject = nodeSequelizeObject;

module.exports = db;
