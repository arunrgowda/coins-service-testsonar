module.exports = function (sequelize, Sequelize) {
  var coin = sequelize.define(
    'coin',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      type: { type: Sequelize.STRING(100), allowNull: true },
      title: { type: Sequelize.STRING, allowNull: false },
      description: { type: Sequelize.STRING, allowNull: false },
      minAmount: { type: Sequelize.INTEGER, allowNull: false },
      maxAmount: { type: Sequelize.INTEGER, allowNull: false },
      validFrom: { type: Sequelize.INTEGER, allowNull: true },
      validTill: { type: Sequelize.INTEGER, allowNull: true },
      purpose: { type: Sequelize.STRING, allowNull: false },
      usageLimit: { type: Sequelize.INTEGER, allowNull: true },
      status: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'coins',
      underScored: false,
      timeStamps: false,
    },
  );
  coin.associate = function (models) {
    coin.hasMany(models.coinsLanguageData);
    coin.hasMany(models.patientsCoin)


  }
  return coin;
};
