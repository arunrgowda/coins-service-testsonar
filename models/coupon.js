module.exports = function (sequelize, Sequelize) {
  var coupon = sequelize.define(
    'coupon',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      couponCode: { type: Sequelize.STRING, allowNull: false },
      rewardId: { type: Sequelize.BIGINT, allowNull: false },
      patientId: { type: Sequelize.BIGINT, allowNull: false },
      usageLimit: { type: Sequelize.INTEGER, allowNull: true },
      purpose: { type: Sequelize.STRING, allowNull: false },
      status: { type: Sequelize.INTEGER, allowNull: false },
      validFrom: { type: Sequelize.DATE, allowNull: true },
      validTill: { type: Sequelize.DATE, allowNull: true },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'coupons',
      underScored: false,
      timeStamps: false,
    },
  );
  coupon.associate = function (models) {
    coupon.hasOne(models.patientsReward)
  }
  return coupon;
};
