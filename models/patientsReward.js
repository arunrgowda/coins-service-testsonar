module.exports = function (sequelize, Sequelize) {
  var patientsReward = sequelize.define(
    'patientsReward',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      couponId: { type: Sequelize.BIGINT, allowNull: true },
      rewardId: { type: Sequelize.BIGINT, allowNull: false },
      patientId: { type: Sequelize.BIGINT, allowNull: false },
      validFrom: { type: Sequelize.DATE, allowNull: true },
      validTill: { type: Sequelize.DATE, allowNull: true },
      status: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'patientsReward',
      underScored: false,
      timeStamps: false,
    },
  );
  patientsReward.associate = function (models) {
    patientsReward.belongsTo(models.reward, {
      foreignKey: 'rewardId',
      targetKey: "id"
    })
    patientsReward.belongsTo(models.coupon, {
      foreignKey: 'couponId',
      targetKey: "id"
    })

  }
  return patientsReward;
};
