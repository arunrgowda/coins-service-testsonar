module.exports = function (sequelize, Sequelize) {
  var patientsCoin = sequelize.define(
    'patientsCoin',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      coinId: { type: Sequelize.BIGINT, allowNull: false },
      patientId: { type: Sequelize.BIGINT, allowNull: false },
      amount: { type: Sequelize.INTEGER, allowNull: false },
      validFrom: { type: Sequelize.DATE, allowNull: true },
      validTill: { type: Sequelize.DATE, allowNull: true },
      status: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        default: Sequelize.NOW,
      }
    },
    {
      tableName: 'patientsCoins',
      underScored: false,
      timeStamps: false,
    },
  );
  patientsCoin.associate = function (models) {
    patientsCoin.belongsTo(models.coin, {
      foreignKey: 'coinId',
      targetKey: "id"
    })

  }
  return patientsCoin;
};
