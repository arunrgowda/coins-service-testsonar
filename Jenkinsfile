def buildTag = "test_v_"+"${env.BUILD_NUMBER}";

def env = "test";
def region = "us-west-2";
def repoName = "coins";
def repoUri = "547208114500.dkr.ecr."+region+".amazonaws.com/"+repoName;

def taskDefFamily = repoName+"-task";
def service = repoName+"-service";
def cluster = "test-cluster-spot";
def containerName = repoName+"-container";
def containerPort = 3000;
def serviceTag = repoName+"/{{.ID}}";
def targetGroupArn = "arn:aws:elasticloadbalancing:us-west-2:547208114500:targetgroup/coins-test-tg/cc85a12d6239631c";

def cpuCapacity = 256;
def memoryCapacity = 512;

def dataCredentials = "data_credentials";
def awsCredentials = "aws_credentials";
def hostConfig = "host_config";
def appConfig = "app_config";

def dataCredentialsSecret = env+"_"+repoName+"_"+dataCredentials;
def awsCredentialsSecret = env+"_"+repoName+"_"+awsCredentials;
def hostConfigSecret = env+"_"+repoName+"_"+hostConfig;
def appConfigSecret = env+"_"+repoName+"_"+appConfig;

def dataCredentialsFile = env+"_"+dataCredentials;
def awsCredentialsFile = env+"_"+awsCredentials;
def hostConfigFile = hostConfig;
def appConfigFile = appConfig;

pipeline{
  agent any
  stages{
    stage('Build Secrets'){
      steps{
        sh """
          mkdir -p config/credentials
          echo "Fetching Credentials"
          #aws secretsmanager get-secret-value --secret-id production_subscription-service | jq -rc .SecretString > .npmrc
          #aws secretsmanager get-secret-value --secret-id ${dataCredentialsSecret} | jq -rc .SecretString > config/credentials/${dataCredentialsFile}.json
          #aws secretsmanager get-secret-value --secret-id ${awsCredentialsSecret} | jq -rc .SecretString > config/credentials/${awsCredentialsFile}.json
          #aws secretsmanager get-secret-value --secret-id ${hostConfigSecret} | jq -rc .SecretString > config/credentials/${hostConfigFile}.json
          #aws secretsmanager get-secret-value --secret-id ${appConfigSecret} | jq -rc .SecretString > config/credentials/${appConfigFile}.json
        """
      }
    }
    stage('Build Image'){
      steps{
        sh """
          sed -e "s;%env%;${env};g;" Dockerfile > Dockerfile-${buildTag}
          docker build -t ${repoName}:${buildTag} -f Dockerfile-${buildTag} --pull=true $WORKSPACE
          docker tag ${repoName}:${buildTag} ${repoUri}:${buildTag}
          DOCKER_LOGIN=`aws ecr get-login --region ${region} | sed -e 's/-e none//g'`
          \${DOCKER_LOGIN}
          docker push ${repoUri}:${buildTag}
        """
      }
    }
    stage('Deploy'){
      input{
        message "Do you want to proceed for deployment?"
      }
      steps{
        sh """
          sed -e "s;%BUILD_TAG%;${buildTag};g;s;%REPO_URI%;${repoUri};g;s;%FAMILY_NAME%;${taskDefFamily};g;s;%CONTAINER_NAME%;${containerName};g;s;%CONTAINER_PORT%;${containerPort};g;s;%SERVICE_TAG%;${serviceTag};g;s;%CPU_CAPACITY%;${cpuCapacity};g;s;%MEMORY_CAPACITY%;${memoryCapacity};g;" taskDef.json > taskDef-${buildTag}.json
          aws ecs register-task-definition --family ${taskDefFamily} --region ${region} --cli-input-json file://taskDef-${buildTag}.json

          # Update the service with the new task definition and desired count
          REVISION=`aws ecs describe-task-definition --task-definition ${taskDefFamily} --region ${region} | jq '.taskDefinition .revision'`
          SERVICES=`aws ecs describe-services --services ${service} --cluster ${cluster} --region ${region} | jq .failures[]`

          #Create or update service
          if [ "\${SERVICES}" = "" ]; then
            echo "Updating the service"
            DESIRED_COUNT=`aws ecs describe-services --services ${service} --cluster ${cluster} --region ${region} | jq .services[].desiredCount`
            if [ \${DESIRED_COUNT} = "0" ]; then
                DESIRED_COUNT="1"
            fi
            aws ecs update-service --cluster ${cluster} --region ${region} --service ${service} --task-definition ${taskDefFamily}:\${REVISION} --desired-count \${DESIRED_COUNT} --deployment-configuration maximumPercent=200,minimumHealthyPercent=100
          else
            echo "Creating a new Service"
            aws ecs create-service --service-name ${service} --desired-count 1 --task-definition ${taskDefFamily} --cluster ${cluster} --region ${region}  --load-balancers targetGroupArn=${targetGroupArn},containerName=${containerName},containerPort=${containerPort}
          fi
          """
          }
        }
  }
}
