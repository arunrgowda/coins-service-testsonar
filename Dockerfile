FROM node:8.11.3-alpine 

WORKDIR /var/app/current


COPY package.json .
RUN npm install

COPY . .

ENV NODE_ENV %env%

CMD ["npm","start"]

EXPOSE 3000
