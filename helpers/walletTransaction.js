var request = require("request-promise");
var baseUrl = require("../config/config")["baseUrl"];

const  makeTransaction = function(patientId,amount,info){
    try {

        var postObject = {};
        postObject["safePassword"] = "7770";
        postObject.patientId = patientId;
        postObject.walletAmount = amount;
        postObject.source = 'reward-coin';
        postObject.type = `reward -${amount<0?'debit':'credit'}`;
        postObject.auto = 0;
        postObject.info = info;
        postObject.identifier="abcdefgh";
        var options = {
            method: 'POST',
            uri: `${baseUrl}dashboardapp/admin/addWalletTransaction`,
            form:postObject,
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            }
        };

        return request(options).then(function(responseData){
            console.log(responseData)
            return responseData?JSON.parse(responseData):{};
        }).catch(function(error){
            console.log(error)
            return error;
        });

  
    }catch(ex){
        console.log(ex);
        return Promise.resolve({success : 0, reason :  ex.message})
    }
}
const  getPatientsDocAppCash = function(patientId){
    try {
        var postObject = {};
        postObject.patientId = patientId;
        var options = {
            method: 'POST',
            uri: `${baseUrl}dashboardapp/patient/getWalletBalance`,
            form:postObject,
            headers: {
                'content-type': 'application/json'
            }
        };

        return request(options).then(function(responseData){
            return responseData;
        }).catch(function(err){
            return err;
        });

  
    }catch(ex){
        console.log(ex);
        return Promise.resolve({success : 0, reason :  ex.message})
    }
}
module.exports={
    makeTransaction,
    getPatientsDocAppCash
}