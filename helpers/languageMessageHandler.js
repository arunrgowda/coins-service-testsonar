const hi = require('../languageData/hi.json');
const en = require('../languageData/en.json');
/**
 * This function is to get the localised message wrt customer's language preference
 * @param {*} languageCode - language
 * @param {*} messageCode - message to be converted
 * @param {*} fromErrorCode - if error
 * @returns language specific string
 */
var getLocalizedMessageForCode = function(
    languageCode,
    messageCode,
  ) {

    let message = '';
    switch (languageCode) {
      case 2: {
        message = hi[messageCode];
        break;
      }

    }
    if (!message) {
      message = en[messageCode];
    }
    return message;
  };
  
  /**
   * This function to send message specific to languageCode.
   * First getting languageCode code from platformDetails
   * call getLocalizedMessageForCode to get LanguageSpecificMessage
   * @param platformDetails
   * @param messageKey
   * @return languageCode specific string if message found else messagekey
   */
  var getLanguageSpecificMessage = (
    languageCode,
    messageKey,
  ) => {
    if (!languageCode){
      languageCode = 'en';
    }
    let localisedMsg = getLocalizedMessageForCode(
      languageCode,
      messageKey,
    );
    if (localisedMsg) {
      return localisedMsg;
    } else {
      return messageKey;
    }
  };

  module.exports = {
    getLanguageSpecificMessage
  }
  