const dbOperations = require('../utils/dbOperations');
const date = require('../utils/date');
const {makeTransaction} = require('../helpers/walletTransaction')
const oneTimeCoinGenerationEvents = ['familyFlow','firstTimeSignUp'];
/* 
This function is to return the list of coins of a patient with respect to patient id
*/

const getPatientsCoinsList = async (req, res) => {
  try {
    const param = req.params;
    if (isNaN(param.patientId)) {
      res.status(400).send({ error: "Invalid request" });
      return;
    }
    let query = {};
    if (isNaN(param.languageCode)) {
      res.status(400).send({ error: "Not a valid language Number" });
      return;
    }
    let languageCode = param.languageCode||"1";
    if (languageCode == '1') {
      query = "SELECT `patientsCoin`.`id` AS `coinId`,`patientsCoin`.`status` AS `status`,patientsCoin.updatedAt,`description` as bottomText," +
        " `title` as header, `patientsCoin`.`validFrom`, `patientsCoin`.`validTill`," +
        " `patientsCoin`.`amount` AS `coinValue`, `purpose`" +
        " FROM `patientsCoins` AS `patientsCoin` INNER JOIN `coins` AS `coin` ON `patientsCoin`.`coinId` = `coin`.`id`"+
        " AND `coin`.`status` = 1 WHERE `patientsCoin`.`patientId` = :patientId AND "+
        " (`patientsCoin`.`status` = 1 OR (`patientsCoin`.`status` = 2 AND `patientsCoin`.`amount` > 0 AND `patientsCoin`.`validTill` > :validTillDate)) "+
        " order by status ASC ,case patientsCoin.status when 1 then  patientsCoin.validTill else patientsCoin.updatedAt end desc; "
      var replacementObj = { patientId: param.patientId ,validTillDate: new Date()}
    } else {
      query = "SELECT `patientsCoins`.`id` AS `coinId`,`patientsCoins`.`status` AS `status`," +
        " CASE " +
        " WHEN `langData`.`description` IS NOT NULL  " +
        " THEN `langData`.`description` " +
        " ELSE `coin`.`description` " +
        " END as bottomText," +
        " CASE " +
        " WHEN `langData`.`title` IS NOT NULL  " +
        " THEN `langData`.`title` " +
        " ELSE `coin`.`title` " +
        " END as header," +
        " `patientsCoins`.`validFrom`, `patientsCoins`.`validTill`," +
        " `amount` AS `coinValue`, `purpose`" +
        " FROM `patientsCoins`  INNER JOIN `coins` AS `coin` ON `patientsCoins`.`coinId` = `coin`.`id` AND  `coin`.`status` = 1" +
        " LEFT JOIN `coinsLanguageData` AS `langData` ON `coin`.`id` = `langData`.`coinId` " +
        "AND `langData`.`languageCode` = :languageCode AND `langData`.`status` = 1  WHERE `patientId` = :patientId AND "+
        "(`patientsCoins`.`status` = 1 OR (`patientsCoins`.`status` = 2 AND `patientsCoins`.`amount` > 0 AND `patientsCoins`.`validTill` > :validTillDate)) "+
        " order by status ASC , case patientsCoins.status when 1 then  patientsCoins.validTill else patientsCoins.updatedAt end desc;"
      var replacementObj = { languageCode:languageCode, patientId: param.patientId ,validTillDate: new Date()}
    }

    const list = await dbOperations.getRawRecordWithSelect('patientsCoin', query, replacementObj);

    res.send({ coinsList: list });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }

}
const activatePatientsCoin = async (req, res) => {
  try {
    let param = req.body;
    let patientCoinId =param.coinId
    if (isNaN(patientCoinId) || isNaN(param.patientId)) {
      res.status(400).send({ error: "Invalid request" });
      return;
    }
    const query = {
      where: {
        id: patientCoinId,
        patientId: param.patientId
      }
    }
    let patientsCoinRecord = await dbOperations.getRawRecords('patientsCoin', query);
    if (!patientsCoinRecord.length) {
      res.status(404).send({ message: "Record not found" });
      return;
    }
    patientsCoinRecord =patientsCoinRecord[0];
    if(patientsCoinRecord.status==2){
      res.status(430).send({ error: "Record already updated" });
      return;
    }
    if(patientsCoinRecord.validFrom>new Date()){
      res.status(430).send({ error: "Coin not available" });
      return;
    }

    if(patientsCoinRecord.validTill<new Date()){
      record = { status: 3 }

    }else{
      let info = `Coin scratched for patientsCoinId id ${patientsCoinRecord.id}`
      let walletTransaction = await makeTransaction(param.patientId,patientsCoinRecord.amount,info);
      if(walletTransaction.success!=1){
        res.status(500).send({ error: walletTransaction.success});//send error to front end
        return;
      }
      record = { status: 2 }

    }
    await dbOperations.updateRecord('patientsCoin', query, record);
    res.send({ message: "Record Udated Successfully" })
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }


}
/**
 * 
 * @param {*} req {patientId,event}
 * @param {*} res 
 */
const generateCoinsForUser = async (req, res) => {
  try {
    const param = req.body;
    let getCoinQuery = {
      where: {
        purpose: param.event,
        status: 1
      }
    }
    if (!param.event || typeof param.event != 'string') {
      console.error({ error: 'Invalid event parameter ' + param.event })
      res.status(400).send({ error: 'Invalid event parameter ' + param.event });
      return;
    }
    if (isNaN(param.patientId)) {
      console.error({ error: 'Invalid patientId parameter ' + param.patientId })
      res.status(400).send({ error: 'Invalid patientId parameter ' + param.patientId });
      return;
    }
    if (param.event.toLowerCase() != 'consultation') {
      if(oneTimeCoinGenerationEvents.indexOf(param.event)>-1 ){

      var isAlreadyGenerated = await checkIsCoinAlreadyGenerated(param.event,param.patientId);
        if(isAlreadyGenerated){
          res.status(400).send({ error: 'Coin already generated before.'});
          return; 
        }
      }
      var coinRecord = await dbOperations.getRawRecords('coin', getCoinQuery);
      if (!coinRecord.length) {
        console.error({ error: 'Coin based on event not found' });
        res.status(400).send({ error: 'Coin based on event not found' });
        return;
      }
      coinRecord = coinRecord[0];
      let startDate = coinRecord.validFrom != null ? new Date(date.addDays(new Date(), coinRecord.validFrom).setHours(0, 0, 0, 0)) : null;
      let endDate = coinRecord.validTill ? new Date(date.addDays(new Date(), coinRecord.validTill).setHours(23, 59, 59, 999)) : null;

      const amount = randomIntInc(coinRecord.minAmount || 0, coinRecord.maxAmount)
      var patientsCoinRecord = {
        coinId: coinRecord.id,
        patientId: param.patientId,
        amount: amount,
        status: 1,
        validFrom: startDate,
        validTill: endDate,
      }
      let createdCoinRecord = await dbOperations.createRecord('patientsCoin', patientsCoinRecord);
      res.send({
        message: "coin created successfully"
      })


    } else {

    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }

}
const checkIsCoinAlreadyGenerated = async(event,patientId)=>{
    let query = {
      attributes:['id'],
      where:{
        patientId:patientId
      },
      include:[{
        model:dbOperations.db.coin,
        attributes:['id'],
        where:{
          purpose:event
        }
      }]

    }
    let patientsCoinRecord = await dbOperations.getRawRecords('patientsCoin', query);
    if(patientsCoinRecord.length>0){
      return true;
    }else{
      return false;
    }

  
}
/**
 * This function generates random integer between two numbers low (inclusive) and high (inclusive) 
 * @param {*} low 
 * @param {*} high 
 */
function randomIntInc(low, high) {
  return Math.floor(Math.random() * (high - low + 1) + low)
}
module.exports = {
  getPatientsCoinsList,
  generateCoinsForUser,
  activatePatientsCoin
}