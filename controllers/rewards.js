/* 
This function is to return the list of rewards
*/
const httpStatus = require('http-status-codes');

const dbOperations = require('../utils/dbOperations');
const { generateCouponCode } = require('../utils/couponCode');
const { makeTransaction } = require('../helpers/walletTransaction')
const date = require('../utils/date');
const models = require("../models");
const nodeDbModels = models['nodeDb'];
const {getLanguageSpecificMessage} = require("../helpers/languageMessageHandler")

/**
 * this fuction is to get reward List for patient
 * @param {*} req 
 *      as query  parameter in url  :patientId
 * @param {*} res 
 */
const getRewardsList = async (req, res) => {
  try {
    const param = req.params;
    if (isNaN(param.patientId)) {
      res.status(400).send({ error: "Invalid request" });
      return;
    }
    if (isNaN(param.languageCode)) {
      res.status(400).send({ error: "Not a valid language Number" });
      return;
    }
    let query = {
      where: { id: param.patientId },
      attributes: ['id', 'version']
    }
    let patientRecord = await dbOperations.getRawRecordsFromNodeDb('Patient', query);
    if (!patientRecord.length) {
      res.status(404).send({ error: "Patient record found" });
      return;
    }
    patientRecord = patientRecord[0];

    let usersLanguageCode = param.languageCode || '1';
    if (usersLanguageCode == '1') {
      query = "select rewards.id as rewardId, title, description,  amount, usageLimit, imageUrl," +
        " colourCode, redirectUrl, rewards.action as `action`, claimedCount from rewards" +
        " left join(SELECT rewards.id as rewardId, count(patientsReward.rewardId) as claimedCount FROM rewards " +
        "left join patientsReward ON rewards.id = patientsReward.rewardId AND patientId= :patientId AND patientsReward.status=1" +
        " WHERE rewards.status=1 AND rewards.version <=:version group by rewards.id) as derivedRewardTable " +
        "ON derivedRewardTable.rewardId = rewards.Id where rewards.status=1 AND rewards.version <=:version"
      var replacementObj = { patientId: param.patientId, version: patientRecord.version }
    } else {
      query = "select rewards.id as rewardId, amount, usageLimit,rewards.imageUrl, case when langData.description IS NOT NULL THEN langData.description" +
        " ELSE rewards.description END AS description," +
        " case when langData.title IS NOT NULL THEN langData.title ELSE rewards.title END AS title," +
        " colourCode, redirectUrl, rewards.action as `action`, claimedCount from rewards left join(SELECT rewards.id as rewardId," +
        " count(patientsReward.rewardId) as claimedCount FROM rewards left join patientsReward ON rewards.id = patientsReward.rewardId " +
        "AND patientId= :patientId AND patientsReward.status=1 WHERE rewards.status=1 AND rewards.version <=:version group by rewards.id) as derivedRewardTable ON " +
        "derivedRewardTable.rewardId = rewards.Id left join rewardsLanguageData as langData ON" +
        " rewards.id = langData.rewardId AND langData.languageCode=:languageCode AND langData.status =1 WHERE rewards.status=1 AND rewards.version <=:version"
      var replacementObj = { patientId: param.patientId, languageCode: usersLanguageCode, version: patientRecord.version }

    }


    const list = await dbOperations.getRawRecordWithSelect('rewards', query, replacementObj);

    res.send({ rewardsList: list });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }
}

/**
 * This function is to mark reward as claimed for user 
 * get coupon code and give in response to user
 * @param {*} req 
 *          rewardId
 *          patientId
 * @param {*} res {message :"",claimedRecord:{(optional)}}
 */

const activateRewardForPatient = async (req, res) => {
  try {
    const param = req.body;

    if (isNaN(param.rewardId) || isNaN(param.patientId) || isNaN(param.languageCode)) {
      res.status(400).send({ error: "Invalid request" });
      return;
    }

    let rewardRecordDetails = await getRewardRecordDetails(param);
    if (!rewardRecordDetails.success) {
      res.status(404).send({ error: "Record not found" });
      return;
    }
    let rewardRecord = rewardRecordDetails.reward;
    /* put check user is not exceeding the limit */
    var claimedRecordCountQuery = {
      rewardId: param.rewardId,
      patientId: param.patientId,
      status: 1
    }
    const claimedCount = await dbOperations.recordCount('patientsReward', claimedRecordCountQuery);
    if (claimedCount >= rewardRecord.usageLimit) {
      res.status(400).send({ error: "Exceeded usage limit of reward" });
      return;
    }
    let info = `Reward claim for reward id ${rewardRecord.id}`
    let walletTransaction = await makeTransaction(param.patientId, -rewardRecord.amount, info);
    if (walletTransaction.success != 1) {
      res.status(500).send({ error: walletTransaction.status });//send error to front end
      return;
    }
    let couponRecord = await createCouponCodeEntry(rewardRecord, param);
    let nodeDbCouponRecord = await createCouponCodeEntryInNodeDB(rewardRecord, couponRecord)
    let record = { rewardId: param.rewardId, patientId: param.patientId, status: 1, couponId: couponRecord.id }
    let createdRecord = await dbOperations.createRecord('patientsReward', record);

    res.send({
      message: "Record Udated Successfully",
      claimedRewards: {
        id: createdRecord.id,
        claimedDate: createdRecord.createdAt,
        colourCode: rewardRecord.colourCode,
        steps: rewardRecord.steps,
        title: rewardRecord.title,
        rewardId: rewardRecord.id,
        couponCode: couponRecord.couponCode
      }
    })
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }
}
const getRewardRecordDetails = async (param) => {
  var query = {
    where: {
      id: param.rewardId,
      status: 1
    }
  }
  let rewardRecord = await dbOperations.getRawRecords('reward', query);
  if (!rewardRecord.length) {
    return { success: 0 }
  }
  rewardRecord = rewardRecord[0];
  if (param.languageCode != 1) {
    query = {
      where: {
        rewardId: param.rewardId,
        status: 1,
        languageCode: param.languageCode
      },
      attributes: ['steps', 'title'],
    }
    let rewardLangRecord = await dbOperations.getRawRecords('rewardsLanguageData', query);
    if (rewardLangRecord.length) {
      rewardRecord.steps = rewardLangRecord[0].steps;
      rewardRecord.title = rewardLangRecord[0].title;
    }
  }
  return {
    success: 1,
    reward: rewardRecord
  }
}
/**
 * This function create coupon record entry in coupon table in node DB after unique check
 * @param {*} reward  
 * @param {*} couponRecord  {couponCode}
 *          
 */
const createCouponCodeEntryInNodeDB = async (reward, couponRecord) => {

  try {
    let record = {
      name: 'Reward',
      code: couponRecord.couponCode,
      couponRule: 1,
      discount: 100,
      reusable: 1,
      globalReusable: 1,
      firstOnly: 0,
      active: 1,
      topic: "all",
      validTill: new Date(date.addDays(new Date(), '365').setHours(23, 59, 59, 999)),
      serviceType: 'nonConsult',
      maximum: reward.amount,
      invalidTopic: '',
      meta: null
    }
    var couponRecord = await dbOperations.createRecordInNodeDb('couponCode', record);
    return couponRecord;
  } catch (error) {

    console.error(error);
    throw error;
  }
}

/**
 * This function create coupon record entry in coupon table after unique check
 * @param {*} reward  
 * @param {*} param  {patientId}
 *          
 */
const createCouponCodeEntry = async (reward, param) => {
  let isCouponRecordCreated = false;
  do {
    try {
      let coupon = generateCouponCode(1)
      let record = {
        rewardId: reward.id, couponCode: coupon[0],
        patientId: param.patientId, status: 1,
        purpose: reward.purpose,
        status: 1
      }
      var couponRecord = await dbOperations.createRecord('coupon', record);
      isCouponRecordCreated = true;
    } catch (error) {

      console.error(error)
      isCouponRecordCreated = false;
    }
  } while (!isCouponRecordCreated)
  return couponRecord;
}
/**
 * This function is to get claimed rewards of patient
 * @param {*} req {patientId}
 * @param {*} res 
 */
const getClaimedRewardList = async (req, res) => {
  try {
    const param = req.params;
    if (isNaN(param.patientId)) {
      res.status(400).send({ error: "Invalid request" });
      return;
    }
    if (isNaN(param.languageCode)) {
      res.status(400).send({ error: "Not a valid language Number" });
      return;
    }
    let usersLanguageCode = param.languageCode || '1';
    if (usersLanguageCode == '1') {

      var claimedRewardListQuery = "SELECT `patientsReward`.`createdAt` AS `claimedDate`, `patientsReward`.`rewardId`, " +
        "`patientsReward`.`id`, `reward`.`title` , `reward`.`colourCode` , `reward`.`steps` , " +
        "`coupon`.`couponCode`  FROM `patientsReward` AS `patientsReward` INNER JOIN `rewards` AS `reward`" +
        " ON `patientsReward`.`rewardId` = `reward`.`id` AND `reward`.`status` = 1 LEFT JOIN `coupons` AS `coupon`" +
        " ON `patientsReward`.`couponId` = `coupon`.`id` AND `coupon`.`status` = 1 WHERE `patientsReward`.`patientId` = :patientId " +
        "AND `patientsReward`.`status` = 1 ORDER BY `patientsReward`.`createdAt` DESC;"
      var replacementObj = { patientId: param.patientId }

    } else {
      var claimedRewardListQuery = "SELECT `patientsReward`.`createdAt` AS `claimedDate`, `patientsReward`.`rewardId`, `patientsReward`.`id`, " +
        " CASE " +
        " WHEN `langData`.`steps` IS NOT NULL  " +
        " THEN `langData`.`steps` " +
        " ELSE `reward`.`steps` " +
        " END as steps," +
        " CASE " +
        " WHEN `langData`.`title` IS NOT NULL  " +
        " THEN `langData`.`title` " +
        " ELSE `reward`.`title` " +
        " END as title," +
        " `reward`.`colourCode` ,  " +
        " `coupon`.`couponCode`  FROM `patientsReward` AS `patientsReward` INNER JOIN `rewards` AS `reward` ON" +
        " `patientsReward`.`rewardId` = `reward`.`id` AND `reward`.`status` = 1 LEFT OUTER JOIN `rewardsLanguageData` AS `langData`" +
        " ON `reward`.`id` = `langData`.`rewardId` AND `langData`.`status` = 1 AND " +
        "`langData`.`languageCode` = :languageCode " +
        "LEFT JOIN `coupons` AS `coupon` ON `patientsReward`.`couponId` = `coupon`.`id` AND `coupon`.`status` = 1 " +
        "WHERE `patientsReward`.`patientId` = :patientId AND `patientsReward`.`status` = 1 ORDER BY `patientsReward`.`createdAt` DESC;"
      var replacementObj = { patientId: param.patientId, languageCode: usersLanguageCode }
    }
    const patientsClaimedRewardList = await dbOperations.getRawRecordWithSelect('patientsReward', claimedRewardListQuery, replacementObj);
    res.send({ claimedRewards: patientsClaimedRewardList })
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Something went wrong" })
  }
}


const moneyTransferToPaytm = async (req, res) => {
  let param = req.body;
  if (param.languageCode && isNaN(param.languageCode)) {
    res.status(400).send({ success:0,message: "Not a valid language Number" });
    return;
  }
  let languageCode = param.languageCode || 1;
  try {
    if(isNaN(param.patientId)||isNaN(param.phoneNumber)|| isNaN(param.rewardId)){
      res.status(httpStatus.BAD_REQUEST).send({ success:0,message: getLanguageSpecificMessage(languageCode, "invalidInput") });
      return;
    }

    let variables = {
      patientId: param.patientId,
      phoneNumber: param.phoneNumber
    };


    // try {
    //   await sessionValidator.validateReq(req, true);
    // } catch (error) {
    //   res.status(httpStatus.UNAUTHORIZED).send({ success: 0, message: getLanguageSpecificMessage(languageCode, "invalidSession") })
    //   return;
    // }


    let query = {
      where: {
        id: param.rewardId,
        status: 1
            },
      attributes: ['amount', 'usageLimit','id'],
    }
    let rewardRecord = await dbOperations.getRawRecords('reward', query);
    if(!rewardRecord.length){
      res.status(404).send({success:0, message: "Reward record found" });
      return;
    }
    rewardRecord=rewardRecord[0];
    var claimedRecordCountQuery = {
      rewardId: param.rewardId,
      patientId: param.patientId,
      status: 1
    }
    const claimedCount = await dbOperations.recordCount('patientsReward', claimedRecordCountQuery);
    if (claimedCount >= rewardRecord.usageLimit) {
      res.status(400).send({ success:0,message: getLanguageSpecificMessage(languageCode, "exceededRewardLimit")});
      return;
    }
    /* getting patient wallet amount */
    let patientRecord = await nodeDbModels.Patient.find({ where: { id: variables.patientId } })
    if(patientRecord==null){
      res.status(404).send({success:0, message: "Patient record found" });
      return;
    }
    let walletAmount = patientRecord && patientRecord["dataValues"] && patientRecord["dataValues"].wallet;
    if (walletAmount < rewardRecord.amount) {
      res.send({ success: 0, message: getLanguageSpecificMessage(languageCode, "insufficientDocsCash") })
      return;
    }
    variables.amount = rewardRecord.amount;
    let {verifyNumberAndTransfer} = require('../service/paytm/paytmTransaction');
    verifyNumberAndTransfer(variables).then((responseBody) => {
      if (responseBody.error) {
        res.send({ success: 0, message: getLanguageSpecificMessage(languageCode, responseBody.error) })
      } else {
        res.send({ success: 1, message: getLanguageSpecificMessage(languageCode, "rewardPaytmTransferSuccessfull") })

      }
    })
      .catch(error => {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ success: 0, message: getLanguageSpecificMessage(languageCode, "internalServerError") })

      });
  } catch (error) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ success: 0, message: getLanguageSpecificMessage(languageCode, "internalServerError") })

  };
}
module.exports = {
  getRewardsList,
  activateRewardForPatient,
  getClaimedRewardList,
  moneyTransferToPaytm
}