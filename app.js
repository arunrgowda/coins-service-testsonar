const env       = process.env.NODE_ENV || "development";

const apmServiceName =  `coinsservice-${env}`;
const apmPublicServerUrl = "http://172.31.11.136:8200";

if(env === "production" || env === "test"){
  var apm = require('elastic-apm-node').start({
    serviceName: apmServiceName,
    secretToken: '',
    serverUrl: apmPublicServerUrl,
    captureSpanStackTraces:false,
    captureBody:"all"
  });
}

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const indexRouter = require('./routes/index');
const coinsRouter = require('./routes/coins');
const rewardsRouter = require('./routes/rewards');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/coins', coinsRouter);
app.use('/rewards', rewardsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
