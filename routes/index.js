var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/health/stats', function(req, res, next) {
  res.send('Coin API - Status Up');
});

module.exports = router;
