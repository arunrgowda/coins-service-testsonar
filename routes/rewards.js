var express = require('express');
var router = express.Router();
const { getRewardsList, activateRewardForPatient, getClaimedRewardList ,moneyTransferToPaytm} = require('../controllers/rewards');

router.get('/list/:languageCode/:patientId', getRewardsList);
router.post('/activate', activateRewardForPatient);
router.get('/claimedRewards/list/:languageCode/:patientId', getClaimedRewardList);
router.post('/paytm/verifyNumberAndTransfer', moneyTransferToPaytm);


module.exports = router;