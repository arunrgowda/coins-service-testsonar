var express = require('express');
const { getPatientsCoinsList, activatePatientsCoin, generateCoinsForUser } = require('../controllers/coins');

var router = express.Router();


router.get('/list/:languageCode/:patientId', getPatientsCoinsList);
router.post('/', generateCoinsForUser);


router.post('/activate', activatePatientsCoin);


module.exports = router;

